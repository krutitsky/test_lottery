import datetime, requests, bs4, threading, csv
from dateutil.relativedelta import relativedelta


results = [["Game", "Draw_Date", "Jackpot_(million)", "Number_1", "Number_2", "Number_3", "Number_4", "Number_5", "Number_6"]]


def get_dates():
	""" Get a list of dates for every Monday for the last 3 months """

	today = datetime.date.today()

	date = today - relativedelta(months=3, weekday=0)
	dates = list()

	while True:
		dates.append(date.strftime("%m/%d/%y"))
		date = date + datetime.timedelta(days=7)
		if date > today:
			return [[dates[i], dates[i + 1]] for i in range(len(dates) - 1)]


def get_response(dates):
	""" Get response for the date range  """

	start, end = dates

	headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0"}

	url = "https://lottery.com/results/us/powerball/?start={start}&end={end}".format(start=start, end=end)

	return requests.get(url, headers=headers, timeout=30)


def get_formatted_data(response):
	""" Format the response and add it to the global list 'results' """

	global results

	soup = bs4.BeautifulSoup(response.text, 'lxml')

	lines = soup.find_all("a", {"class": "lottery-vertical-view-items"})

	for line in lines:

		result = list()

		result.append(line.find("div", {"class": "title-column"}).string)
		result.append(line['href'].split("/")[:-1][-1:][0])
		result.append(line.find("div", {"class": "jackpot-column"}).find("span").text.split(" ")[0][1:])
		result += [number.string for number in line.find_all("span", {"class": "lottery-number"})]

		results.append(result)


def csv_writer(data):
	"""	Write data to CSV """

	with open("result.csv", "w", newline='') as csv_file:
		writer = csv.writer(csv_file, delimiter='\t')
		for line in data:
			writer.writerow(line)


def get_and_format_response(date):
	""" Get the response and format it """

	response = get_response(date)
	get_formatted_data(response)


def main():

	dates = get_dates()

	thread_list = []

	for date in dates:
		thread = threading.Thread(target=get_and_format_response, args=(date, ))
		thread_list.append(thread)
		thread.start()

	for thread in thread_list:
		thread.join()

	csv_writer(results)


if __name__ == "__main__":
	main()
